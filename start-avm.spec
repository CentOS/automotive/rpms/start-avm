%global forgeurl https://gitlab.com/CentOS/automotive/src/start-avm
%global tag 0.1.0

Summary: Tools to start Android VMs with QEMU
Name: start-avm
Version: %{tag}
%forgemeta

Release: 3%{?dist}
URL:     %{forgeurl}
Source:  %{forgeurl}/-/archive/%{tag}/%{name}-%{tag}.tar.bz2
License: BSD-3-Clause

BuildRequires: gcc
BuildRequires: make
BuildRequires: git

%description
Tools to start Android VMs with QEMU

%prep
%autosetup -S git

%build
%{make_build}

%install
%{make_install}
mkdir -p %{buildroot}%{_datadir}/start_avm
cp -r templates %{buildroot}%{_datadir}/start_avm

%files
%doc README.md
%license COPYING
%{_bindir}/start_avm
%{_bindir}/start_cvd_tools
%{_datadir}/start_avm/templates/.cuttlefish_config.json

%changelog
* Wed Mar 13 2024 Sandro Bonazzola <sbonazzo@redhat.com> - 0.1.0-3
- Initial import
